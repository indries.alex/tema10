<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Car;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class CarController extends AbstractController
{
    /**
     * @Route("/car", name="car")
     */
    public function new(Request $request)
    {

        $car = new Car();

        $form = $this->createFormBuilder($car)
        ->add('marca', TextType::class)
        ->add('culoare', TextType::class)
        ->add('save', SubmitType::class, ['label' => 'Create Car'])
        ->getForm();

        $form->handleRequest($request);
        $succes = 0;

        if ($form->isSubmitted()) {
            $car = $form->getData();
            $succes = 1;
            $enitityManager = $this->getDoctrine()->getManager();
            $enitityManager->persist($car);
            $enitityManager->flush();
        } else {
            $succes = 2;
        }

        return $this->render('car/index.html.twig', [
            'form' => $form->createView(),
            'succes' => $succes,
        ]); 
    }
}
